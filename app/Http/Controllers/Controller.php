<?php

namespace App\Http\Controllers;

use App\Services\Weather\Contracts\WeatherService;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    protected WeatherService $weatherData;

    public function __construct(WeatherService $weatherData)
    {
        $this->weatherData = $weatherData;
    }

    /**
     * @return View
     */
    public function index(): View
    {
        return view('forecast');
    }

    /**
     * @param Request $request
     * @return View
     */
    public function getCityTemp(Request $request): View
    {
        $forecast = $this->weatherData->find($request->search);

        return view('forecast')->with(compact('forecast'));
    }
}
