<?php

namespace App\Services\Weather\Contracts;

use App\Services\Weather\Weather;

interface WeatherService
{
    /**
     * @param $city
     * @return Weather
     */
    public function find($city): Weather;
}
