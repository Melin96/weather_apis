<?php

namespace App\Services\Weather;

use App\Services\Weather\Contracts\WeatherPartner;
use App\Services\Weather\WeatherPartners\WeatherPartnerFirst;
use App\Services\Weather\WeatherPartners\WeatherPartnerSecond;
use App\Services\Weather\WeatherPartners\WeatherPartnerThird;
use App\Services\Weather\Contracts\WeatherFacade as WeatherFacadeInterface;

class WeatherFacade implements WeatherFacadeInterface
{
    /**
     * @var WeatherPartner
     */
    private WeatherPartner $partnerFirst;

    /**
     * @var WeatherPartner
     */
    private WeatherPartner $partnerSecond;

    /**
     * @var WeatherPartner
     */
    private WeatherPartner $partnerThird;

    /**
     * @inheritDoc
     */
    public function getPartnerFirst(): WeatherPartner
    {
        if (!isset($this->partnerFirst)) {
            $this->partnerFirst = new WeatherPartnerFirst();
        }

        return $this->partnerFirst;
    }

    /**
     * @inheritDoc
     */
    public function getPartnerSecond(): WeatherPartner
    {
        if (!isset($this->partnerSecond)) {
            $this->partnerSecond = new WeatherPartnerSecond();
        }

        return $this->partnerSecond;
    }

    /**
     * @inheritDoc
     */
    public function getPartnerThird(): WeatherPartner
    {
        if (!isset($this->partnerThird)) {
            $this->partnerThird = new WeatherPartnerThird();
        }

        return $this->partnerThird;
    }
}
