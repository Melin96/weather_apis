<?php

namespace App\Services\Weather;

use App\Services\Weather\Contracts\WeatherComposite;

class Weather extends WeatherFilter implements WeatherComposite
{
    /**
     * @var array
     */
    protected array $items;

    /**
     * @var string
     */
    protected string $city;

    /**
     * @param $weatherComposite
     */
    public function setItems($weatherComposite): void
    {
        $eachCount = count($weatherComposite[0]);
        $allCount = count($weatherComposite);
        $this->items = [];

        for ($i = 0; $i < $eachCount; $i++) {
            for ($j=0; $j < $allCount; $j++) {
                $this->items[$i]['time'] = $weatherComposite[$j][$i]['time'];

                //Set average temperature in Celsius and Fahrenheit
                if (isset($data[$i]['value'])) {
                    $this->items[$i]['value']['celsius'] += $this->countAverage($weatherComposite[$j][$i]['value'], $allCount);
                    $this->items[$i]['value']['fahrenheit'] += $this->celsiusToFahrenheit($this->items[$i]['value']['celsius']);
                } else {
                    $this->items[$i]['value']['celsius'] = $this->countAverage($weatherComposite[$j][$i]['value'], $allCount);
                    $this->items[$i]['value']['fahrenheit'] = $this->celsiusToFahrenheit($this->items[$i]['value']['celsius']);
                }
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function getItems(): array
    {
        return $this->items;
    }

    /**
     * @param $city
     */
    public function setCity($city): void
    {
        $this->city = $city;
    }

    /**
     * @inheritDoc
     */
    public function getCity(): string
    {
        return $this->city;
    }
}
