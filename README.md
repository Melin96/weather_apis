Installation

- composer install
- create .env file from env example
- php artisan key:generate
- npm install
- npm run dev
- php artisan serve

For testing, you can search for Amsterdam.
