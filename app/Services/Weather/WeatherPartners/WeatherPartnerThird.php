<?php

namespace App\Services\Weather\WeatherPartners;

use App\Services\Weather\Contracts\WeatherPartner;
use App\Services\Weather\WeatherFilter;
use League\Csv\Reader;

class WeatherPartnerThird extends WeatherFilter implements WeatherPartner
{
    /**
     * @inheritDoc
     */
    public function getWeather($city): array
    {
        $reader = Reader::createFromPath(public_path('temperatures/temps.json'), 'r');
        $data = json_decode($reader->getContent(), true);

        return $this->filter($data['predictions'], $city);
    }
}
