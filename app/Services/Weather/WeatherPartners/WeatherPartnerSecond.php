<?php

namespace App\Services\Weather\WeatherPartners;

use App\Services\Weather\Contracts\WeatherPartner;
use App\Services\Weather\WeatherFilter;

class WeatherPartnerSecond extends WeatherFilter implements WeatherPartner
{
    /**
     * @inheritDoc
     */
    public function getWeather($city): array
    {
        $json = file_get_contents(public_path('temperatures/temps.json'));
        $data = json_decode($json, true);

        return $this->filter($data['predictions'], $city);
    }

}
