<?php

namespace App\Services\Weather\Contracts;

interface WeatherPartner
{
    /**
     * @param $city
     * @return array
     */
    public function getWeather($city): array;
}
