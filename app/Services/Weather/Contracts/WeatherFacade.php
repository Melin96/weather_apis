<?php

namespace App\Services\Weather\Contracts;

interface WeatherFacade
{
    /**
     * @return WeatherPartner
     */
    public function getPartnerFirst(): WeatherPartner;

    /**
     * @return WeatherPartner
     */
    public function getPartnerSecond(): WeatherPartner;

    /**
     * @return WeatherPartner
     */
    public function getPartnerThird(): WeatherPartner;
}
