<?php

namespace App\Services\Weather\Contracts;

/**
 * Weather Composite.
 */
interface WeatherComposite
{
    /**
     * @return WeatherComposite[]
     */
    public function getItems(): array;

    /**
     * @return string
     */
    public function getCity(): string;
}
