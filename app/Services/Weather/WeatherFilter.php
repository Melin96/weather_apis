<?php

namespace App\Services\Weather;

class WeatherFilter
{
    /**
     * @param $data
     * @param $city
     * @return array
     */
    protected function filter($data, $city): array
    {
        $predictions = [];

        if (strtolower($data['city']) == strtolower($city) && $data['prediction']) {
            $predictions = $data['prediction'];

            foreach ($predictions as &$prediction) {
                $fahrenheit = $prediction['value'];
                $prediction['value'] = $this->fahrenheitToCelsius($fahrenheit);
            }
        }

        return $predictions;
    }

    /**
     * @param float $fahrenheit
     * @return float
     */
    protected function fahrenheitToCelsius(float $fahrenheit): float
    {
        return (int)round(($fahrenheit - 32) / 1.8);
    }

    /**
     * @param float $celsius
     * @return int
     */
    protected function celsiusToFahrenheit(float $celsius): int
    {
        return (int)round($celsius * 1.8 + 32);
    }

    /**
     * @param $value
     * @param $count
     * @return int
     */
    protected function countAverage($value, $count): int
    {
        return (int)round($value / $count);
    }
}
