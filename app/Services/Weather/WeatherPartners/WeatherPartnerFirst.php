<?php

namespace App\Services\Weather\WeatherPartners;

use App\Services\Weather\Contracts\WeatherPartner;

class WeatherPartnerFirst implements WeatherPartner
{
    /**
     * @inheritDoc
     */
    public function getWeather($city): array
    {
        $predictions = [];

        $xmlString = file_get_contents(public_path('temperatures/temps.xml'));
        $xmlObject = simplexml_load_string($xmlString);

        $json = json_encode($xmlObject);
        $data = json_decode($json, true);

        if (strtolower($data['city']) == strtolower($city) && $data['prediction']) {
            $predictions = $data['prediction'];
        }

        return $predictions;
    }

}
