<?php

namespace App\Providers;

use App\Services\Weather\WeatherFacade;
use App\Services\Weather\WeatherService;
use App\Services\Weather\Contracts\WeatherFacade as WeatherFacadeInterface;
use App\Services\Weather\Contracts\WeatherService as WeatherServiceInterface;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(WeatherFacadeInterface::class, WeatherFacade::class);
        $this->app->singleton(WeatherServiceInterface::class, WeatherService::class);
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
}
