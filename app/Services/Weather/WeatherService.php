<?php

namespace App\Services\Weather;

use App\Services\Weather\Contracts\WeatherFacade as WeatherFacadeInterface;
use App\Services\Weather\Contracts\WeatherPartner;
use App\Services\Weather\Contracts\WeatherService as WeatherServiceInterface;

class WeatherService implements WeatherServiceInterface, WeatherFacadeInterface
{
    /**
     * @var WeatherFacadeInterface
     */
    private WeatherFacadeInterface $weatherFacade;

    public function __construct(WeatherFacadeInterface $weatherFacade)
    {
        $this->weatherFacade = $weatherFacade;
    }

    /**
     * @inheritDoc
     */
    public function find($city): Weather
    {
        $weather = new Weather();

        $weather->setItems([
            $this->getPartnerFirst()->getWeather($city),
            $this->getPartnerSecond()->getWeather($city),
            $this->getPartnerThird()->getWeather($city)
        ]);

        $weather->setCity($city);

        return $weather;
    }

    /**
     * @return WeatherPartner
     */
    public function getPartnerFirst(): WeatherPartner
    {
        return $this->weatherFacade->getPartnerFirst();
    }

    /**
     * @return WeatherPartner
     */
    public function getPartnerSecond(): WeatherPartner
    {
        return $this->weatherFacade->getPartnerSecond();
    }

    /**
     * @return WeatherPartner
     */
    public function getPartnerThird(): WeatherPartner
    {
        return $this->weatherFacade->getPartnerThird();
    }
}
