<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Weather</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">

    <script src="{{ asset('js/app.js') }}" defer></script>
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">

    <style>
        body {
            font-family: 'Nunito', sans-serif;
        }
    </style>
</head>
<body>
    <div class="container">
        <form action="{{ route('search') }}" method="GET">
            <div class="input-group">
                <div class="form-outline col-md-8">
                    <input placeholder="Enter city" value="{{ (isset($forecast) && $forecast->getCity()) ? $forecast->getCity() : '' }}" name="search" id="search-focus" type="search" class="form-control" required/>
                </div>
                <button type="submit" class="btn btn-primary">
                    Search
                </button>
            </div>
        </form>
        @if (isset($forecast) && count($forecast->getItems()))
            <table class="table">
                <thead>
                <tr>
                    <th scope="col"> Hour </th>
                    <th scope="col"> Temperature (Celsius) </th>
                    <th scope="col"> Temperature (Fahrenheit) </th>
                </tr>
                </thead>
                <tbody>
                @foreach ($forecast->getItems() as $f)
                    <tr>
                        <td> {{ $f['time'] }} </td>
                        <td> {{ $f['value']['celsius'] }}&deg;C </td>
                        <td> {{ $f['value']['fahrenheit'] }}&deg;F </td>
                    </tr>
                @endforeach
                </tbody>
            </table>
        @else
            <li>Sorry my dear friend, no forecast here.</li>
        @endif
    </div>
</body>
</html>
